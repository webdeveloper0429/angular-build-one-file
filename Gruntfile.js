module.exports = function (grunt) {
    grunt.initConfig({
        concat_sourcemap: {
            options: {
                sourcesContent: true,
            },
            js: {
                files: {
                    'build/js/main.js': ['dist/my-demo/runtime-*.js', 'dist/my-demo/polyfills-*.js', 'dist/my-demo/*.js']
                }
            },
            css: {
                files: {
                    'build/css/main.css': 'dist/my-demo/*.css'
                }
            }
        },
        watch: {
            scripts: {
                files: ['dist/my-demo/*.js', 'dist/my-demo/*.css'],
                tasks: ['concat_sourcemap'],
                options: {
                    spawn: false,
                },
            },
        }
    });

    grunt.loadNpmTasks('grunt-concat-sourcemap');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build', ['concat_sourcemap']);
};